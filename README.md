# Shining Bake Test

**Disclaimer**: This project is archived: development has been stopped in April 2019 and is open sourced only for reference and sharing. **We provide no support on it**. Right now it would require Vertigo software (which has not been Open Sourced yet by Ubisoft) to generate Shining scenes from VDB files. So this repository is only pushed here as reference and for the sake of completeness (to open source all repositories related to Shining).

---

Repository which contains bake script and Vertigo scenes (with their media) to execute __Shining Bake Test NRU__

This script can be executed __only on Windows__, because it uses Vertigo software

Owns __shining_render_test__ repository as a __submodule__. 

Python script scans the json files in this submodule, and output __bioc/json files__ that can be use by the __Shining Render Test NRU__.